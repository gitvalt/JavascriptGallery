# Javascript gallery

## Developer
Valtteri Seuranen aka. gitvalt

## Introduction
This project was to create a simple javascript gallery with React js, webpack, bootstrap, SASS and Jquery, while the code would be writen with 
Coffeescript.

## Table of content

    dist/   Finished product shown in a html page. 
        image_mocks/    Folder for images shown in gallery  
        index.html      Mainpage
        gallery-min.js  The app packed with Webpack
    export/ gallery in minified format
    src/    App files
        coffeescript/
        js/
        style/
        drink_coffee    Script for building all coffeescript into javascript files
    build   Build web.config.js
    docker-compose.yml
    package.json
    web.config.js

## Running
Install docker and docker-composer and then run docker-compose.yml.
Docker should start nginx-server at localhost:80.

## Export
Minified version of the gallery component at export -folder (does not include styling or app.js component)

To use Gallery component 
    Import gallery.js
    Create a React.ComponentFactory in your code with imported gallery.js
