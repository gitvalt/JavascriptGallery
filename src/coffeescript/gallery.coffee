
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap'

React = require('react')
ReactDOM = require('react-dom')

# Creates a simple Gallery with react js and bootstrap
class Gallery extends React.Component
    constructor: (props) ->
        super props
        @state = {
            pictureIndex: 0,    #Which picture is currenlty shown
            alert: null,        #shown alert
            maxPaginators: 5,  #how many paginator can be shown at screen
        }
    
    componentWillMount: ->
        if @props.pictures not instanceof Array or @props.pictures.length is 0
            err = "Images not defined"
            @state.alert = err
            console.error err
        null

    # Get picture index and change it with value of "change". Then return new value
    # Pictures 3 pieces --> index rotation = 0 -> 1 -> 2 -> 0 -> 1 ...
    getNewPicIndex: (change) ->
        if @props.debug
            console.log "Gallery.getNewPicIndex: Index change with value #{change}"
        
        i = @state.pictureIndex + change

        max = @props.pictures.length - 1
        if i < 0
            i = max
        else if i > max
            i = 0

        return i
    
    # Change currently shown picture
    changePictureIndex: (changeTo) ->
        if @props.debug
            console.log "Gallery.changeTo: Changing index to #{changeTo}"
        
        if changeTo is null
            @setAlert("Gallery.changeTo: ChangeTo variable is null")
            return

        @setState({ pictureIndex: changeTo })

    # Give image index when jumping to index $int
    # (example:
    # 10 pictures in slides and current image index is 9,
    # is asked what is the index of a image 30 steps past current one)
    # calculation: 9 -> 10 -> 0 -> 1 -> 2 ... 9 -> 10 -> 0 ... until we find out image is index 9
    parseFromCurrentIndex: (int) ->
        count = int
        result = 0
        modulo = Math.abs(int) % (@props.pictures.length)
        if int < 0
            result = @state.maxPaginators - modulo
        else
            result = modulo

    # When hovered div is clicked, change the shown image.
    onHoverDivClick: (origin, e) ->

        if @props.debug
            console.log "Gallery.onHover: Event fired '#{origin}', '#{@state.pictureIndex}'"

        if @props.pictures is null
            console.log "Gallery.onHover: Cannot change picture, no pictures defined"
            return

        if origin is null
            @setAlert("Gallery.onHover: Origin not defined")

        switch origin
            when "left" then @changePictureIndex(@getNewPicIndex(-1))
            when "right" then @changePictureIndex(@getNewPicIndex(1))
            else @setAlert("Gallery.onHover: Unkown origin")

    # When mouse hovered over div, show the div panel
    onHoverDivHoverIn: (e) ->
        if @props.debug
            console.log "Gallery.onHoverDivHover: Mouse hovered over div"
        
        e.target.style.opacity = 0.6
    
    # When mouse is moved away from hovered div, hide the panel
    onHoverDivHoverOut: (e) ->
        if @props.debug
            console.log "Gallery.onHoverDivHover: Mouse hovered out of div"
        
        e.target.style.opacity = 0

    # When Paginator button is clicked
    onPaginatorClick: (origin, e) ->
        prefix = "Gallery.onPagigatorClick: "
        e.preventDefault()

        if @props.debug
            console.log prefix + "Paginator link clicked. Value: '#{origin}''"
        
        if origin is null
            @setAlert(prefix + "onClick origin is null")

        switch origin
            when "previous" then @changePictureIndex(@getNewPicIndex(-1))
            when "next" then @changePictureIndex(@getNewPicIndex(1))
            else
                # Change image based on number shown in paginator
                if Number.isInteger(origin)
                    @changePictureIndex(origin)
                    null
                else
                    @setAlert(prefix +
                    "Origin is not a valid variable (not previous, next or a integer)")
                    null

    #set the currenlty displayed alert
    setAlert: (message) ->
        if @props.debug
            console.log "Gallery.setAlert: Changing alert message"
            console.log message

        if message is null
            console.error "Gallery.setAlert: Message is empty"
            return

        @setState({ alert: message })

    handleAlert: () ->
       if @state.alert != null
            "alert alert-danger"
        else
            null

    #render image with index @state.pictureIndex
    renderImage: (images, imgIndex) ->
        prefix = "Gallery.renderImages: "
        if @props.debug
            console.log prefix + "Rendering images from props"

        if images not instanceof Array
            console.error prefix + "Cannot render images. Images not a array"
            @state.alert = "Props.pictures: Images not a array"
            return null

        if images.length is 0 or images is null
            console.error prefix + "Cannot render images. No images defined"
            @state.alert = "Props.pictures: No images defined"
            return null

        # if given index is out of picture arrays bounds
        path = images[imgIndex]
        if path is undefined or path is null
            console.error prefix + "Cannot render images. Selected image not found"
            @state.alert = "Props.pictures: Selected image not found"
            return null

        image = React.createElement("img", {
            src: path,
            alt: path,
            className: "gallery_image"
        })

        # return created img
        image

    # render the gallery paginator
    renderPaginator: () ->
        if @props.debug
            console.log "Gallery.renderPaginator: Rendering paginator"

        # how many pictures are there and what is paginator's startingpoint and endpoint when:
        # max number paginator button is @state.maxPaginators
        length = @props.pictures.length
        min = 0
        max = @state.maxPaginators - 1

        if length < 0
            @state.alert = "Gallery.renderPaginator: Pictures out of bounds"
            return null
        else if length is 0
            @state.alert = "Gallery.renderPaginator: No pictures defined"
            return null

        #if there are less pictures than max amount allowed -> all can be shown -> 0...max
        if length < @state.maxPaginators
            max = length - 1 #array starts at 0

        else if length >= @state.maxPaginators
            min = @state.pictureIndex
            max = @state.pictureIndex + @state.maxPaginators
            #example: amount of pictures 18 --> min is 0 and max 10...
        
        pagi = (index, array) ->
            React.createElement "li",
                { className: "page-item", key: "page_" + index },
                React.createElement "a", {
                    className: "page-link",
                    onClick: (e) => @onPaginatorClick(@parseFromCurrentIndex(index), e)
                    href: "#"
                    }, @parseFromCurrentIndex(index)
        .bind(@)
      
        rows = []

        # Creates previous, next and (min to max) -paginator buttons
        React.createElement "ul", { className: "pagination justify-content-center" },
                    React.createElement "li", { className: "page-item", key: "prev" },
                        React.createElement "a", {
                            className: "page-link",
                            href: "#",
                            onClick: (e) => @onPaginatorClick("previous", e)
                            }, "Previous"
                    pagi i, rows for i in [min .. max]
                    React.createElement "li", { className: "page-item", key: "next" },
                        React.createElement "a", {
                            className: "page-link",
                            href: "#",
                            onClick: (e) => @onPaginatorClick("next", e)
                            }, "Next"

    render: ->

        @state.alert = null
        paginator = @renderPaginator()
        currentImage = @renderImage(@props.pictures, @state.pictureIndex)

        React.createElement "div", { id: "gallery_Box" },
            React.createElement "div", {
                id: "gallery_alertbox",
                className: "row"
                },
                React.createElement "div", {
                    id: "gallery_alert",
                    className: @handleAlert(),
                    role: "alert"
                    }, @state.alert
            React.createElement "div", { id: "gallery_slider", className: "row" },
                React.createElement "div", { id: "slider_imageBox" },
                    React.createElement "div", {
                        id: "gallery_slider_left",
                        onClick: (e) => @onHoverDivClick("left", e),
                        onMouseOver: (e) => @onHoverDivHoverIn(e),
                        onMouseLeave: (e) => @onHoverDivHoverOut(e)
                        }, null
                    currentImage,
                    React.createElement "div", {
                        id: "gallery_slider_right",
                        onClick: (e) => @onHoverDivClick("right", e),
                        onMouseOver: (e) => @onHoverDivHoverIn(e),
                        onMouseLeave: (e) => @onHoverDivHoverOut(e)
                        }, null
            React.createElement "div", { id: "gallery_paginator", className: "row" },
                paginator

module.exports = Gallery
