import * as React from 'react'
import ReactDom from 'react-dom'
import "../style/gallery.scss"

#Get list of mock images from dist/image_mocks
images = [
    "./image_mocks/img.png",
    "./image_mocks/img_2.svg",
    "./image_mocks/img_3.png",
    "./image_mocks/img_4.jpg",
    "./image_mocks/No-logo.svg",
]

#Create element from react class and render it to "header" div.
Component = React.createFactory require("./gallery.js")
ReactDom.render Component({ pictures: images, debug: true }), document.getElementById('header')

