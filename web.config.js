const path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: './src/js/app.js',
  cache: true,
  mode: "development",
  output: {
    filename: 'gallery-min.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules:
      [
        {
          test: [/\.js$/, /\.es6$/, /\.jsx$/],
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['es2015', 'babel-preset-react']
            }
          }
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader']
        },
        {
          test: /\.(jpg|png|svg)$/,
          use: {
            loader: "url-loader",
            options: {
              limit: 25000,
            },
          },
        },
        {
          test: /\.scss$/,
          use: ['style-loader', 'css-loader', 'sass-loader']
        }

      ]
  }
};
